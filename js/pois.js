var pois = [    
    {
        "type": "Feature",
        "properties": {
            "name": "M&R Angelgeräte GmbH",
            "type": "store"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [8.4566109, 48.9956547]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": "Angelcenter Karlsruhe - Fishingtackle24",
            "type": "store"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [8.352469,49.023147]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": "Meyers Angelladen",
            "type": "store"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [8.320897,49.018354]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": "Big Fish Angelmarkt",
            "type": "store"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [8.682776,48.898288]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": "American Tackle Shop",
            "type": "store"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [8.457773,49.232985]
        }
    }
];