<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2">
  <xsl:output method="text"/>

   <xsl:template match="/">
      <xsl:apply-templates select="kml:kml/kml:Document/kml:Folder/kml:Placemark"/>
   </xsl:template>

   <xsl:template match="kml:Placemark">
         {
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [<xsl:value-of select="kml:Point"/>]
	},
	"properties": {
    "name": "<xsl:value-of select="kml:name"/>",
"type": "parking"
  }
},         
   </xsl:template>

</xsl:stylesheet>
